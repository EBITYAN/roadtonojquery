(() => {
    'use strict';

    const fadeIn = function (node, duration, callBackOnEnd) {
        let fadeDuration = duration ? duration : 500;
        const start = performance.now();

        if (!node) {
            throw new Error('target fadeIn object was not found');
        }
        if (window.getComputedStyle(node).display !== 'none') {
            return;
        }

        if (node.style.display === 'none') {
            node.style.display = '';
        } else {
            node.style.display = 'block';
        }
        node.style.opacity = 0;

        window.requestAnimationFrame(function init(timestamp) {
            let easing = (timestamp - start) / fadeDuration;

            node.style.opacity = Math.min(easing, 1);
            if (easing < 1) {
                window.requestAnimationFrame(init);
            } else {
                node.style.opacity = '';
                if (typeof callBackOnEnd !== undefined) {
                    // フェードイン完了後フォールバック
                    callBackOnEnd();
                }
            }
        });
    };

    const fadeOut = function (node, duration, callBackOnEnd) {
        let fadeDuration = duration ? duration : 500;
        const start = performance.now();

        if (!node) {
            throw new Error('target fadeIn object was not found');
        }

        window.requestAnimationFrame(function init(timestamp) {
            let easing = (timestamp - start) / fadeDuration;

            node.style.opacity = Math.max(1 - easing, 0);
            if (easing < 1) {
                requestAnimationFrame(init);
            } else {
                node.style.opacity = '';
                node.style.display = 'none';
                if (typeof callBackOnEnd !== undefined) {
                    // フェードイン完了後フォールバック
                    callBackOnEnd();
                }
            }
        });
    };

    let sampleFadeIn = document.querySelector('.sample-fadeIn');
    let sampleFadeOut = document.querySelector('.sample-fadeOut');

    fadeIn(sampleFadeIn, null, function () {
        sampleFadeIn.style.background = 'pink';
        sampleFadeIn.innerHTML = '<p>fadeInが終わりました</p>';
        console.log('fadeInが終わりました');
    });

    fadeOut(sampleFadeOut, 1500, function () {
        console.log('fadeInが終わりました');
    });
})((function () {
    'use strict';

    let elemCode = document.querySelectorAll('.code');

    elemCode.forEach((node) => {
        let self = node;
        let preBlock = node.querySelector('pre');
        let codeBlock = node.querySelector('code');
        let button = document.createElement('button');

        button.type = 'button';
        button.classList.add('btn-code-open');
        button.innerText = 'コードブロックを展開';

        button.addEventListener('click', function () {
            let position = self.offsetTop;

            if (this.classList.contains('is-open')) {
                // Close
                this.classList.remove('is-open');
                preBlock.style.maxHeight = '';
                codeBlock.style.maxHeight = '';
                this.innerText = 'コードブロックを展開';
                window.scrollTo(0, position);

                return;
            }
            this.classList.add('is-open');
            this.innerText = 'コードブロックを閉じる';
            preBlock.style.maxHeight = 'none';
            codeBlock.style.maxHeight = 'none';
        });

        self.after(button);
    });
}()));
